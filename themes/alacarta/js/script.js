$(document).ready(function(){

    var categorias = {};

    onCategoria = function(e){
        e.preventDefault();

        var categoria = $(this).text();

        $(".view-plato .plato").addClass("d-none");

        $(".view-plato .plato[data-categoria="+categoria+"]").removeClass("d-none");

        console.log(this);

    };

    $(".view-plato .plato").each(function(i, p){
        var categoria = $(p).find(".views-field-field-categoria .field-content").text();
        categorias[categoria] = categoria;

        $(p).attr("data-categoria", categoria);
        console.log(categoria, p);
    });

    var categorias_menu = "";
    var ul = document.createElement('ul');
    $(ul).addClass("nav");
    Object.keys(categorias).forEach(k => {
        var c = categorias[k];
        var li = document.createElement("li");
        $(li).addClass("nav-item");

        var a = document.createElement("a");
        $(a).addClass('nav-link');
        $(a).text(c);
        $(a).attr("href","#");
        $(a).on('click', onCategoria);

        $(li).append(a);
        $(ul).append(li);
        
        
    });


    $(".view-plato .categorias").html(ul);
    $(".view-plato .categorias a.nav-link").eq(0).click();

 

});