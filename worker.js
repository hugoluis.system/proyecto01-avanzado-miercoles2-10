const CACHE_NAME = '20190926';

const FILES_TO_CACHE = [
    '/offline',
    '/sites/default/themes/alacarta/css/bootstrap.min.css?pyif5u',
    'sites/default/themes/alacarta/js/jquery-3.3.1.slim.min.js?pyif5u'
];


self.addEventListener('install', (evt) => {
    console.log('[ServiceWorker] Install');
    // CODELAB: Precache static resources here.
    evt.waitUntil(
        caches.open(CACHE_NAME)
            .then((cache)=> {
                console.log("Cache Loaded");
                return cache.addAll(FILES_TO_CACHE);
            })
    );

    self.skipWaiting();
  });




  self.addEventListener('activate', (evt) => {
    console.log('[ServiceWorker] Activate');
    // CODELAB: Remove previous cached data from disk.
    evt.waitUntil(
        caches.keys().then((keyList) => {
          return Promise.all(keyList.map((key) => {
            if (key !== CACHE_NAME) {
              console.log('[ServiceWorker] Removing old cache', key);
              return caches.delete(key);
            }
          }));
        })
    );

    self.clients.claim();
  });


  self.addEventListener('fetch', (evt) => {
    console.log('[ServiceWorker] Fetch', evt.request.url);
    // CODELAB: Add fetch event handler here.
    if (evt.request.mode !== 'navigate') {
        // Not a page navigation, bail.
        return;
      }
      evt.respondWith(
          fetch(evt.request)
              .catch(() => {
                return caches.open(CACHE_NAME)
                    .then((cache) => {
                        console.log("FROM CACHE");
                      return cache.match('offline');
                    });
              })
      );
    
    
    
  });

  